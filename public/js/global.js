webpackJsonp([0],[
/* 0 */,
/* 1 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function($, jQuery, __webpack_provided_window_dot_jQuery) {

__webpack_require__(2);

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance"); }

function _iterableToArray(iter) { if (Symbol.iterator in Object(iter) || Object.prototype.toString.call(iter) === "[object Arguments]") return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = new Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } }

//============================
//    Name: index.js
//============================
// import for others scripts to use
window.$ = $;
__webpack_provided_window_dot_jQuery = jQuery;
// import 'babel-polyfill';
// import 'owl.carousel';
// import 'jquery-scrollify/jquery.scrollify';
$(function () {
  /* ======= Global Actions ======= */
  var k = 'click',
      act = 'item-active';
  /* ======= ---- ======= */

  /* ======= list ======= */

  var ___order__list_ = function ___order__list_() {
    pageMain.__load();
  };
  /* ======= ---- ======= */


  var pageMain = {
    __load: function __load() {
      // let regexpEmail = /\S+[@]\S+[.]\w+$/gsi;
      var siteName = '.js__imageSwitcherLabel .brief-group-a__radio';
      var iniMarket = ['.js__countMarket', '.js__countMarketNumb'];
      var iniSocial = ['.js__countSocial', '.js__countSocialNumb'];
      var setting = {
        // * Run functions once
        __firstRuns: function __firstRuns() {
          this.setContainer();
          this.creatDots();
          this.socialBuild();
          this.countPageItems.apply(this, [1].concat(iniMarket));
          this.countPageItems.apply(this, [1].concat(iniSocial));
          this.phoneMask();
          this.priceTotalFirst();
        },
        // * Handlers
        __handlers: function __handlers() {
          var _this = this;

          $(window).on('scroll', function () {
            return _this.scrollUPShow();
          });
          $('.js__scrollUP').on(k, function () {
            return $(window).scrollTop(0);
          });
          $("".concat(siteName)).on(k, function (event) {
            return _this.imageSwitcher(event);
          });
          $('.js__startForm').children().on(k, function (event) {
            return _this.startForm(event);
          });
          $('.js__btnBack').children().on(k, function () {
            return _this.btnBack();
          });
          $('.js__btnNext').children().on(k, function () {
            return _this.btnNext();
          });
          $('.js__countMarket input').on(k, function () {
            return _this.countPageItems.apply(_this, [1].concat(iniMarket));
          });
          $('.js__countMarketSelectAll').on(k, function () {
            return _this.countPageItems.apply(_this, [0].concat(iniMarket));
          });
          $('.js__countSocial input').on(k, function () {
            return _this.countPageItems.apply(_this, [1].concat(iniSocial));
          });
          $('.js__countSocialSelectAll').on(k, function () {
            return _this.countPageItems.apply(_this, [0].concat(iniSocial));
          });
          $('.js__btnSenderData').on(k, function () {
            return _this.collectData();
          });
          $('.js__dataLock').on(k, function () {
            return _this.dataLockWarning();
          });
        },

        /* ======= Code ======= */
        sendForm: function sendForm(form_data) {
          var waitBtn = function waitBtn() {
            $('.js__btnSenderData').addClass('btn__color--disable');
            $('.js__btnSenderData').attr('disabled', 'disabled');
          };

          var doneBtn = function doneBtn() {
            $('.js__btnSenderData').removeClass('btn__color--disable');
            $('.js__btnSenderData').addClass('btn__color--green');
            $('.js__btnSenderData').text('Отправлено'); // $('.js__btnSenderData').removeAttr('disabled', 'disabled');
          };

          $.ajax({
            url: 'email-sender.php',
            type: 'POST',
            dataType: 'html',
            data: {
              'site_url': window.location.href,
              'send_name': $('#form-id-name').val(),
              'send_email': $('#form-id-email').val(),
              'send_param': "".concat(form_data)
            }
          }).done(function () {
            return console.log('OKAY');
          }, setTimeout(function () {
            return doneBtn();
          }, 100)).fail(function () {
            return console.log('ERROR');
          }, setTimeout(function () {
            return doneBtn();
          }, 1000)).always(function () {
            return waitBtn();
          });
        },
        dataLockWarning: function dataLockWarning() {
          $('.js__dataLock').is(':visible:checked') ? $('.js__dataLock').next().removeClass('brief__lodash--red') : '';
        },
        collectData: function collectData() {
          var arr = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : '';
          var dataLock = 0,
              dataValue = 0,
              count = $('.js__priceBuild').children().length,
              title = $('.js__priceBuild .brief-price__title'),
              select = $('.js__priceBuild .brief-price__selected');

          for (var i = 0; i < count; i++) {
            arr += title.eq(i).text();
            arr += select.eq(i).children().text();
          }

          arr.toString();

          var warning = function warning() {
            dataLock = 0;
            $('.js__dataLock').next().addClass('brief__lodash--red');
          };

          $('#form-id-name').val().length >= 1 && $('#form-id-email').val().length >= 5 ? dataValue = 1 : dataValue = 0;
          $('.js__dataLock').is(':visible:checked') ? dataLock = 1 : warning();
          dataLock === 1 && dataValue === 1 ? this.sendForm(arr) : '';
        },
        phoneMask: function phoneMask() {
          $('.js__phoneMask').inputmask({
            'mask': '+375 (99) 999 99 99',
            'placeholder': '-'
          });
        },
        plusPrice: function plusPrice() {
          var sum = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 0;
          var x = $('.brief-group__item input:checked').not('.js__dataLock');

          for (var i = 0; i < x.length; i++) {
            sum += Number.parseInt(x.eq(i).val());
          }

          console.log(sum);
          return sum;
        },
        priceTotalFirst: function priceTotalFirst() {
          var numbTitle = $('.brief-group__item').length - 1;

          var x = function x(n) {
            return $('.brief-group__item').eq(n).attr('data-form-title').trim();
          };

          var elem = function elem(title) {
            $('.js__priceBuild').append("\n\t\t\t\t\t\t\t<div class=\"brief__item brief-group-d__item\">\n\t\t\t\t\t\t\t\t<div class=\"brief-price__title\">".concat(title, ": \n</div>\n\t\t\t\t\t\t\t\t<div class=\"brief-price__selected\">\n\t\t\t\t\t\t\t\t\t<div class=\"brief-price__item\">__EMPTY__</div>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t"));
          };

          for (var i = 0; i < numbTitle; i++) {
            elem(x(i));
          }
        },
        priceTotal: function priceTotal() {
          var item = function item(n) {
            return $('.brief-group__item').eq(n).find('input:checked');
          };

          var x = function x(n) {
            return item(n).parent().text().trim();
          };

          var w = function w(n, m) {
            return item(n).parent().find('span').eq(m).text().trim();
          };

          var stage = function stage(x) {
            return $('.js__priceBuild .brief__item').eq(x);
          };

          stage(0).children().eq(1).html("<div class=\"brief-price__item\">".concat(x(0), " \n</div>"));
          stage(1).children().eq(1).html('');

          for (var i = 0; i < item(1).length; i++) {
            stage(1).children().eq(1).append("<div class=\"brief-price__item\">".concat(w(1, i * 2), " \n</div>"));
          }

          stage(2).children().eq(1).html('');

          for (var _i = 1; _i < item(2).length * 2; _i = _i + 2) {
            stage(2).children().eq(1).append("<div class=\"brief-price__item\">".concat(w(2, _i), " \n</div>"));
          }

          stage(3).children().eq(1).html("<div class=\"brief-price__item brief-price__item--final\">$".concat(this.plusPrice(), " \n</div>"));
        },
        socialBuild: function socialBuild() {
          var item = function item() {
            return $('.js__socialContent').text().replace(/\s+/gmi, ' ').trim().split(' ');
          };

          var arr = _toConsumableArray(item());

          var elem = function elem(name, i) {
            $('.js__socialBuild').append("\n\t\t\t\t\t\t<div class=\"brief__item\">\n\t\t\t\t\t\t\t<input id=\"brief-c-checkbox-".concat(i, "\" class=\"brief__input brief-group-c__chebox\" type=\"checkbox\" name=\"checkbox\" value=\"30\">\n\t\t\t\t\t\t\t<label for=\"brief-c-checkbox-").concat(i, "\" class=\"brief__label brief-group-c__label\">\n\t\t\t\t\t\t\t<span class=\"brief-group-c__span\"><svg role=\"img\" class=\"icon__social-").concat(i, "\"></svg></span>\n\t\t\t\t\t\t\t<span class=\"brief-group-c__span-text\">").concat(name, "</span>\n\t\t\t\t\t\t\t</label>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t"));
          };

          arr.forEach(function (name, i) {
            return elem(name, i + 1);
          });
        },
        countPageItems: function countPageItems(path, father, number) {
          var name = "".concat(father, " .brief__item"),
              x = function x() {
            return $(name).has(':checked').length;
          },
              y = function y() {
            return $("".concat(father, " input")).parent('.brief__item').length;
          },
              sum = function sum() {
            return $("".concat(number)).text("".concat(x(), " / ").concat(y()));
          };

          var selectAll = function selectAll() {
            return x() === y() ? '' : $(name).children('input').prop('checked', 'true') && sum();
          };

          path === 1 ? sum() : selectAll();
        },
        setTitle: function setTitle() {
          var myTitle = function myTitle() {
            return $('.js__formContent').children().has(':visible').attr('data-form-title');
          };

          $('.js__formTitle').children().first().text(myTitle());
        },
        creatDots: function creatDots() {
          var createElem = function createElem() {
            return $('.js__formDots').append('<div class="brief-nav__dot">&nbsp;</div>');
          };

          var elLength = function elLength() {
            return $('.js__formContent').children().length - 1;
          };

          for (var i = 0; i < elLength(); i++) {
            createElem();
          }
        },
        shortFunc: function shortFunc(name) {
          $(name).children().removeClass(act);
        },
        setDots: function setDots() {
          this.shortFunc('.js__formDots');
          $('.js__formDots').children().eq(this.takeContainer()).addClass(act);
        },
        btnBack: function btnBack() {
          var _this2 = this;

          var z = function z() {
            return $('.js__btnBack').children();
          };

          var x = function x() {
            return $('.js__btnNext').children();
          };

          var okay = function okay() {
            _this2.shortFunc('.js__formContent');

            $('.js__formContent').children().eq(_this2.takeContainer()).prev().addClass(act);

            _this2.setContainer();

            _this2.setDots();

            _this2.setTitle();
          };

          this.takeContainer() <= 0 ? '' : okay();
          this.takeContainer() <= 0 ? z().addClass('btn__color--disable') && z().attr('disabled', 'disabled') : x().removeClass('btn__color--disable') && x().removeAttr('disabled', 'disabled');
        },
        btnNext: function btnNext() {
          var _this3 = this;

          var z = function z() {
            return $('.js__btnBack').children();
          };

          var x = function x() {
            return $('.js__btnNext').children();
          };

          var elLength = function elLength() {
            return $('.js__formContent').children().length - 1;
          };

          var okay = function okay() {
            _this3.shortFunc('.js__formContent');

            $('.js__formContent').children().eq(_this3.takeContainer()).next().addClass(act);

            _this3.setContainer();

            _this3.setDots();

            _this3.setTitle();
          };

          this.takeContainer() >= elLength() ? '' : okay();
          this.takeContainer() === 3 ? this.priceTotal() : '';
          this.takeContainer() >= 4 ? x().addClass('btn__color--disable') && x().attr('disabled', 'disabled') : z().removeClass('btn__color--disable') && z().removeAttr('disabled', 'disabled');
        },
        setContainer: function setContainer() {
          var tab = function tab() {
            return $('.js__formContent').children().has(':visible').index();
          };

          $('[data-form-index]').attr('data-form-index', tab);
        },
        takeContainer: function takeContainer() {
          var ini = function ini() {
            return $('[data-form-index]').attr('data-form-index');
          };

          return Number.parseInt(ini());
        },
        startForm: function startForm(event) {
          var _this4 = this;

          var nextStage = function nextStage() {
            $('.js__startForm').attr('data-form-open', 'false');
            $('.js__firstScreenForm').attr('data-form-open', 'true'); // $('[data-form-open="false"').removeClass(act);
            // $('[data-form-open="true"').addClass(act);

            _this4.setContainer();

            _this4.setTitle();

            $('.js__btnBack').children().addClass('btn__color--disable') && $('.js__btnBack').children().attr('disabled', 'disabled');
          };

          var skipStage = function skipStage() {
            return console.log('skipStage');
          };

          $(event.currentTarget).index() === 0 ? nextStage() : skipStage();
        },
        imageSwitcher: function imageSwitcher(event) {
          var action = function action(cb) {
            $("".concat(siteName)).parent().removeClass(act);
            $("".concat(siteName, ":checked")).parent().addClass(act);
            cb();
          };

          var next1 = function next1() {
            var qwe = $('.js__imageSwitcherLabel').children('.item-active').index();
            $('.js__imageSwitcher').children().removeClass(act);
            $('.js__imageSwitcher').children().eq(qwe).addClass(act);
          };

          $(event.currentTarget).parent().hasClass(act) ? '' : action(next1);
        },
        scrollUPShow: function scrollUPShow() {
          $(window).scrollTop() >= 100 ? $('.js__scrollUP').removeClass(act + '-out') + $('.js__scrollUP').addClass(act) : $('.js__scrollUP').addClass(act + '-out') + $('.js__scrollUP').removeClass(act);
        },

        /* ======= ---- ======= */
        // * Loaders 
        __loaders: function __loaders() {
          this.__firstRuns();

          this.__handlers();
        }
      };

      setting.__loaders();
    }
  };
  /* ======= list ======= */

  ___order__list_();
  /* ======= ---- ======= */

});
/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(0), __webpack_require__(0), __webpack_require__(0)))

/***/ })
],[1]);
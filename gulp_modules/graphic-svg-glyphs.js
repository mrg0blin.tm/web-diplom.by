/* ==== IMPORT PARAMS ==== */
'use strict';
/* ==== ----- ==== */


/* ==== Sources and directions for files ==== */
const
inDev = 'development',
inDevApps = `${inDev}/components`,
inDevTmp = `${inDev}/tmp`;
/* ==== ----- ==== */



const __cfg = {
	svgmin: {
		js2svg: { pretty: true },
		plugins: [
			{ removeDoctype: true },
			{ removeComments: false },
			{ removeXMLProcInst: true },
			{ cleanupNumericValues: { floatPrecision: 2 } },
			{ convertColors: { names2hex: false, rgb2hex: false } },
			{ removeTitle: true },
			{ removeUselessStrokeAndFill: true },
			{ removeDesc: { removeAny: true } },
			{ convertTransform: true }
		],
	},
	cheerio: { 
		run: ($) => {
				// 'fill',  <- color fill attr for svg sprte
				let arr = ['fill', 'stroke', 'style'];
				arr.forEach((item) => $(`[${item}]`).removeAttr(item) );
			},
			parserOptions: { xmlMode: true }
		},
	svgSymbols: {
			slug: { separator: '-', },
			id: 'id-glyphs-%f',
			class: '$%f$',
			templates: ['default-svg', 'default-stylus', ],
			svgAttrs: { class: 'inline-svg__init', 'aria-hidden': 'true', version: '1.1' }
		}
};

module.exports = ( nameTask, _run, combiner, src, dest, isDevelopment, isPublic, errorConfig ) =>
	() => combiner(
	
		src(`${inDevApps}/img/__glyphs/**/*.svg`, { allowEmpty: true }),
		_run.svgmin(__cfg.svgmin),
		_run.cheerio(__cfg.cheerio),
		_run.replace('&gt;', '>'),
		_run.svgSymbols(__cfg.svgSymbols),
		dest(`${inDevTmp}`)
			
			).on('error',
	_run.notify.onError((err) => errorConfig(`task: ${nameTask} `, 'ошибка!', err)));


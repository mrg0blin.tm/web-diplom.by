/* ==== IMPORT PARAMS ==== */
'use strict';
import { lastRun } from 'gulp';
/* ==== ----- ==== */


/* ==== Sources and directions for files ==== */
const
inDev = 'development';
/* ==== ----- ==== */


/* ==== Replace URL or Links ==== */
const __link = {
	srcpath: {
		in:  'src="../components/img/',
		out:  'src="./media/img/' 
	}
};
/* ==== ----- ==== */


let sinceReplace = (x) => `${x}`.replace(/-/gi, ':');

module.exports = ( nameTask, _run, combiner, src, dest, isDevelopment, isPublic, errorConfig ) =>
	() => combiner(
	
 
			src(`${inDev}/*.html`, { since: lastRun(sinceReplace(nameTask))}),
			_run.htmlReplace(__link.srcpath.in, __link.srcpath.out),
			dest(inDev)) &&
		combiner(
			src(`${inDev}/layouts/**/*.html`),
			_run.htmlReplace(__link.srcpath.in, __link.srcpath.out),
			dest(`${inDev}/layouts`)
			
			
			).on('error',
	_run.notify.onError((err) => errorConfig(`task: ${nameTask} `, 'ошибка!', err)));

  
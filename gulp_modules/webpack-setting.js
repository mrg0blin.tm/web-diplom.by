/* ==== IMPORT PARAMS ==== */
'use strict';
import webpackStream from 'webpack-stream';
import path from 'path';
/* ==== ----- ==== */


/* ==== SOURCES AND DIRECTIONS FOR FILES ==== */
const
	inDev = 'development',
	inDevApps = `${inDev}/components`,
	inPub = 'public',
	inPubJs = `${inPub}/js`,
	dir = `${__dirname}\\..\\`;
/* ==== ----- ==== */


/* ==== WEBPACK INIT ==== */
const noDevState = !process.env.NODE_ENV	|| process.env.NODE_ENV === inDev;
const webpack = webpackStream.webpack;
const webpackAddCustomPlugins = { /* picker: path.resolve('./node_modules/pickadate/lib/picker')*/ };
const PATHS = {
	root: path.join(dir, ''),
	app: path.join(dir, `${inDevApps}/js/`),
	glob: path.join(dir, `${inDevApps}/js/global`),
	cache: path.resolve(dir, `${inDevApps}/js/cached_uglify/`)
};
/* ==== ----- ==== */


/* ==== OTHER SETTINGS==== */
const __other = {
	compress: {
		warnings: false,
		pure_getters: true,
		unsafe: true,
		unsafe_comps: true,
		screw_ie8: true
	},
	ignorelist: [/\.min\.js$/gi, 'global.js'],
};
/* ==== ----- ==== */


/* ==== REPLACE URL OR LINKS ==== */
const __cfg = {
	devtool: noDevState ? 'cheap-module-inline-source-map' : false,
	stats: {
		colors: true,
		modules: true,
		reasons: true,
		errorDetails: true
	},
	babel: [{
		test: /\.js$/,
		exclude: /node_modules/,
		use: { loader: 'babel-loader', options: { presets: ['@babel/preset-env'] } }
	}],
	plugins: {
		provide: { '$': 'jquery', 'jQuery': 'jquery',	'window.jQuery': 'jquery' },
		uglify: {
			cacheFolder: PATHS.cache,
			debug: false,
			minimize: true,
			sourceMap: true,
			mangle: true,
			compress: __other.compress,
			output: { comments: false, },
			exclude: __other.ignorelist,
		},
		chunk: {
			name: 'plugins',
			filename: 'plugins.js',
			minChunks: module => module.context && module.context.indexOf('node_modules') !== -1
		},
	}
};
/* ==== ----- ==== */


const firstRun = true;
const init = {
	entry: { global: PATHS.glob},
	output: { path: PATHS.root, filename: '[name].js', sourceMapFilename: '[name].map'	},
	stats: __cfg.stats,
	watch: noDevState,
	devtool: __cfg.devtool,
	module: { rules: __cfg.babel	},
	resolve: { modules: ['node_modules'],		extensions: ['.js'],		alias: webpackAddCustomPlugins	},
	plugins: [
		new webpack.DefinePlugin(noDevState),
		new webpack.optimize.OccurrenceOrderPlugin(),
		new webpack.NoEmitOnErrorsPlugin(),
		new webpack.ProvidePlugin(__cfg.plugins.provide),
		new webpack.optimize.UglifyJsPlugin(__cfg.plugins.uglify),
		new webpack.optimize.CommonsChunkPlugin(__cfg.plugins.chunk)
	]
};


module.exports = ( nameTask, _run, combiner, src, dest, isDevelopment, isPublic, errorConfig ) =>
	(flag) => (
		combiner(
			src(`${inDevApps}/js/**/*.js`), webpackStream(init),
			dest(inPubJs).on('data', () => firstRun ? flag() : null || '')
		)
	).on('error',
	_run.notify.onError((err) => errorConfig(`task: ${nameTask} `, 'ошибка!', err)));


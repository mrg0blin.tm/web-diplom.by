//============================
//    Name: index.js
//============================

// import for others scripts to use
window.$ = $;
window.jQuery = jQuery;

import 'inputmask/dist/jquery.inputmask.bundle';

// import 'babel-polyfill';
// import 'owl.carousel';
// import 'jquery-scrollify/jquery.scrollify';

$(() => {

	/* ======= Global Actions ======= */
	const
		k = 'click',
		act = 'item-active';
	/* ======= ---- ======= */



	/* ======= list ======= */
	const ___order__list_ = () => {
		pageMain.__load();
	};
	/* ======= ---- ======= */

	const pageMain = {
		__load() {
			// let regexpEmail = /\S+[@]\S+[.]\w+$/gsi;
			let siteName = '.js__imageSwitcherLabel .brief-group-a__radio';
			let iniMarket = ['.js__countMarket', '.js__countMarketNumb'];
			let iniSocial = ['.js__countSocial', '.js__countSocialNumb'];

			let setting = {

				// * Run functions once
				__firstRuns() {
					this.setContainer();
					this.creatDots();
					this.socialBuild();
					this.countPageItems(1, ...iniMarket);
					this.countPageItems(1, ...iniSocial);
					this.phoneMask();
					this.priceTotalFirst();
				},
				// * Handlers
				__handlers() {
					$(window).on('scroll', () => this.scrollUPShow());
					$('.js__scrollUP').on(k, () => $(window).scrollTop(0));
					$(`${siteName}`).on(k, (event) => this.imageSwitcher(event));
					$('.js__startForm').children().on(k, (event) => this.startForm(event));
					$('.js__btnBack').children().on(k, () => this.btnBack());
					$('.js__btnNext').children().on(k, () => this.btnNext());
					$('.js__countMarket input').on(k, () => this.countPageItems(1, ...iniMarket));
					$('.js__countMarketSelectAll').on(k, () => this.countPageItems(0, ...iniMarket));
					$('.js__countSocial input').on(k, () => this.countPageItems(1, ...iniSocial));
					$('.js__countSocialSelectAll').on(k, () => this.countPageItems(0, ...iniSocial));
					$('.js__btnSenderData').on(k, () => this.collectData());
					$('.js__dataLock').on(k, () => this.dataLockWarning());
				},

				/* ======= Code ======= */

				sendForm(form_data) {

					let waitBtn = () => {
						$('.js__btnSenderData').addClass('btn__color--disable');
						$('.js__btnSenderData').attr('disabled', 'disabled');
					};

					let doneBtn = () => {
						$('.js__btnSenderData').removeClass('btn__color--disable');
						$('.js__btnSenderData').addClass('btn__color--green');
						$('.js__btnSenderData').text('Отправлено');
						// $('.js__btnSenderData').removeAttr('disabled', 'disabled');

					};

					$.ajax({
							url: 'email-sender.php',
							type: 'POST',
							dataType: 'html',
							data: {
								'site_url': window.location.href,
								'send_name': $('#form-id-name').val(),
								'send_email': $('#form-id-email').val(),
								'send_param': `${form_data}`
							},
						})
						.done(() => console.log('OKAY'), setTimeout(() => doneBtn(), 100))
						.fail(() => console.log('ERROR'), setTimeout(() => doneBtn(), 1000))
						.always(() => waitBtn());
				},

				dataLockWarning() {
					$('.js__dataLock').is(':visible:checked')
						? $('.js__dataLock').next().removeClass('brief__lodash--red')
						: '';
				},

				collectData(arr = '') {
					let
						dataLock = 0,
						dataValue = 0,
						count = $('.js__priceBuild').children().length,
						title = $('.js__priceBuild .brief-price__title'),
						select = $('.js__priceBuild .brief-price__selected');

					for (let i = 0; i < count; i++) {
						arr += title.eq(i).text();
						arr += select.eq(i).children().text();
					}
					arr.toString();

					let warning = () => {
						dataLock = 0;
						$('.js__dataLock').next().addClass('brief__lodash--red');
					};

					$('#form-id-name').val().length >= 1 && $('#form-id-email').val().length >= 5
						? dataValue = 1
						: dataValue = 0;


					$('.js__dataLock').is(':visible:checked')
						? dataLock = 1
						: warning();


					dataLock === 1 && dataValue === 1
						? this.sendForm(arr)
						: '';
				},

				phoneMask() {
					$('.js__phoneMask').inputmask({
						'mask': '+375 (99) 999 99 99',
						'placeholder': '-'
					});
				},

				plusPrice(sum = 0) {
					let x = $('.brief-group__item input:checked').not('.js__dataLock');
					for (let i = 0; i < x.length; i++) {
						sum += Number.parseInt(x.eq(i).val());
					}
					console.log(sum);
					return sum;
				},


				priceTotalFirst() {
					let numbTitle = $('.brief-group__item').length - 1;
					let x = (n) => $('.brief-group__item').eq(n).attr('data-form-title').trim();

					let elem = (title) => {
						$('.js__priceBuild').append(`
							<div class="brief__item brief-group-d__item">
								<div class="brief-price__title">${title}: \n</div>
								<div class="brief-price__selected">
									<div class="brief-price__item">__EMPTY__</div>
								</div>
							</div>
							`);
					};

					for (let i = 0; i < numbTitle; i++) { elem(x(i)); }
				},

				priceTotal() {
					let item = (n) => $('.brief-group__item').eq(n).find('input:checked');
					let x = (n) => item(n).parent().text().trim();
					let w = (n, m) => item(n).parent().find('span').eq(m).text().trim();
					let stage = (x) => $('.js__priceBuild .brief__item').eq(x);


					stage(0).children().eq(1).html(`<div class="brief-price__item">${x(0)} \n</div>`);

					stage(1).children().eq(1).html('');
					for (let i = 0; i < item(1).length; i++) {
						stage(1).children().eq(1).append(`<div class="brief-price__item">${w(1, i * 2)} \n</div>`);
					}

					stage(2).children().eq(1).html('');
					for (let i = 1; i < item(2).length * 2; i = i + 2) {
						stage(2).children().eq(1).append(`<div class="brief-price__item">${w(2, i)} \n</div>`);
					}

					stage(3).children().eq(1).html(`<div class="brief-price__item brief-price__item--final">$${this.plusPrice()} \n</div>`);

				},

				socialBuild() {
					let item = () => $('.js__socialContent').text().replace(/\s+/gmi, ' ').trim().split(' ');
					let arr = [...item()];

					let elem = (name, i) => {
						$('.js__socialBuild').append(`
						<div class="brief__item">
							<input id="brief-c-checkbox-${i}" class="brief__input brief-group-c__chebox" type="checkbox" name="checkbox" value="30">
							<label for="brief-c-checkbox-${i}" class="brief__label brief-group-c__label">
							<span class="brief-group-c__span"><svg role="img" class="icon__social-${i}"></svg></span>
							<span class="brief-group-c__span-text">${name}</span>
							</label>
						</div>
						`);
					};

					arr.forEach((name, i) => elem(name, i + 1));
				},


				countPageItems(path, father, number) {
					let name = `${father} .brief__item`,
						x = () => $(name).has(':checked').length,
						y = () => $(`${father} input`).parent('.brief__item').length,
						sum = () => $(`${number}`).text(`${x()} / ${y()}`);

					let selectAll = () => x() === y()
						? ''
						: $(name).children('input').prop('checked', 'true') && sum();

					path === 1 ? sum() : selectAll();
				},

				setTitle() {
					let myTitle = () => $('.js__formContent').children().has(':visible').attr('data-form-title');
					$('.js__formTitle').children().first().text(myTitle());
				},

				creatDots() {
					let createElem = () => $('.js__formDots').append('<div class="brief-nav__dot">&nbsp;</div>');
					let elLength = () => $('.js__formContent').children().length - 1;

					for (let i = 0; i < elLength(); i++) { createElem(); }
				},


				shortFunc(name) {
					$(name).children().removeClass(act);
				},


				setDots() {
					this.shortFunc('.js__formDots');
					$('.js__formDots').children().eq(this.takeContainer()).addClass(act);
				},

				btnBack() {
					let z = () => $('.js__btnBack').children();
					let x = () => $('.js__btnNext').children();


					let okay = () => {
						this.shortFunc('.js__formContent');
						$('.js__formContent').children().eq(this.takeContainer()).prev().addClass(act);
						this.setContainer();
						this.setDots();
						this.setTitle();
					};

					this.takeContainer() <= 0 ? '' : okay();

					this.takeContainer() <= 0
						? z().addClass('btn__color--disable') && z().attr('disabled', 'disabled')
						: x().removeClass('btn__color--disable') && x().removeAttr('disabled', 'disabled');



				},

				btnNext() {

					let z = () => $('.js__btnBack').children();
					let x = () => $('.js__btnNext').children();


					let elLength = () => $('.js__formContent').children().length - 1;
					let okay = () => {
						this.shortFunc('.js__formContent');
						$('.js__formContent').children().eq(this.takeContainer()).next().addClass(act);
						this.setContainer();
						this.setDots();
						this.setTitle();
					};

					this.takeContainer() >= elLength() ? '' : okay();
					this.takeContainer() === 3 ? this.priceTotal() : '';


					this.takeContainer() >= 4
						? x().addClass('btn__color--disable') && x().attr('disabled', 'disabled')
						: z().removeClass('btn__color--disable') && z().removeAttr('disabled', 'disabled');


				},

				setContainer() {
					let tab = () => $('.js__formContent').children().has(':visible').index();
					$('[data-form-index]').attr('data-form-index', tab);
				},

				takeContainer() {
					let ini = () => $('[data-form-index]').attr('data-form-index');
					return Number.parseInt(ini());
				},


				startForm(event) {

					let nextStage = () => {

						$('.js__startForm').attr('data-form-open', 'false');
						$('.js__firstScreenForm').attr('data-form-open', 'true');

						// $('[data-form-open="false"').removeClass(act);
						// $('[data-form-open="true"').addClass(act);

						this.setContainer();
						this.setTitle();

						$('.js__btnBack').children().addClass('btn__color--disable') && $('.js__btnBack').children().attr('disabled', 'disabled');

					};

					let skipStage = () => console.log('skipStage');

					$(event.currentTarget).index() === 0
						? nextStage() : skipStage();

				},


				imageSwitcher(event) {


					let action = (cb) => {
						$(`${siteName}`).parent().removeClass(act);
						$(`${siteName}:checked`).parent().addClass(act);
						cb();
					};

					let next1 = () => {
						let qwe = $('.js__imageSwitcherLabel').children('.item-active').index();
						$('.js__imageSwitcher').children().removeClass(act);
						$('.js__imageSwitcher').children().eq(qwe).addClass(act);
					};

					$(event.currentTarget).parent().hasClass(act) ? '' : action(next1);


				},



				scrollUPShow() {
					$(window).scrollTop() >= 100
						? $('.js__scrollUP').removeClass(act + '-out') + $('.js__scrollUP').addClass(act)
						: $('.js__scrollUP').addClass(act + '-out') + $('.js__scrollUP').removeClass(act);
				},

				/* ======= ---- ======= */




				// * Loaders 
				__loaders() {
					this.__firstRuns();
					this.__handlers();
				}
			};
			setting.__loaders();
		}
	};
	/* ======= list ======= */
	___order__list_();
	/* ======= ---- ======= */
});